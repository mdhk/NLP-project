# do required imports
import torch
from torch.autograd import Variable
import torch.nn as nn
import pickle
import dill
import numpy as np
from collections import defaultdict, Counter

lm = torch.load('model.pt', map_location=lambda storage, loc: storage)
# print(type(lm))
# Load dictionary word --> id
dictionary = pickle.load(open('dict', 'rb'))


# set the maximum sequence length
max_seq_len = 50

# function to transform sentence into word id's and put them in a pytorch Variable
# NB Assumes the sentence is already tokenised!
def tokenise(sentence, dictionary):
    words = sentence.split(' ')
    l = len(words)
    assert l <= max_seq_len, "sentence too long"
    token = 0
    ids = torch.LongTensor(l)

    for word in words:
        try:
            ids[token] = dictionary.word2idx[word]
        except KeyError:
            print( word)
            raw_input()
            ids[token] = dictionary.word2idx['<unk>']
        token += 1
    return ids

# load Penn Treebank data
penntree_sentences = [sentence.split() for sentence in open('sec02-21.gold.tagged.txt', 'r').readlines()]

# generate the word_pos_dict such that word_pos_dict[word][pos] = count
word_pos_dict = {}
for sentence in penntree_sentences:
    for token in sentence:
        word = token.split('|')[0].lower()
        pos = token.split('|')[1]
        if word in word_pos_dict:
            if pos in word_pos_dict[word]:
                word_pos_dict[word][pos] += 1
            else:
                word_pos_dict[word][pos] = 1
        else:
            word_pos_dict[word] = {pos: 1}

word_pos_dict_limited = {}
for sentence in penntree_sentences:
    for token in sentence:
        word = token.split('|')[0].lower()
        pos = token.split('|')[1]
        if word not in dictionary.word2idx:
            continue
        else:
            if word in word_pos_dict_limited:
                if pos in word_pos_dict_limited[word]:
                    word_pos_dict_limited[word][pos] += 1
                else:
                    word_pos_dict_limited[word][pos] = 1
            else:
                word_pos_dict_limited[word] = {pos: 1}


# convert the word_pos_dict into a pos_word_dict such that pos_word_dict[pos][word] = count
pos_word_dict = {}
for word in word_pos_dict:
    # skip the words that are not in the model dictionary
    if word not in dictionary.word2idx:
        continue
    else:
        for pos, count in word_pos_dict[word].items():
            if pos in pos_word_dict:
                if word in pos_word_dict[pos]:
                    pos_word_dict[pos][word] += count
                else:
                    pos_word_dict[pos][word] = count
            else:
                pos_word_dict[pos] = {word: count}

# convert counts to (unsmoothed) probabilities per tag, sort by probability
for pos in pos_word_dict:
    total = sum([count for word, count in pos_word_dict[pos].items()])
    word_freqs = []
    for word, count in pos_word_dict[pos].items():
        word_freq = (word, (count/total))
        word_freqs.append(word_freq)
    pos_word_dict[pos] = sorted(word_freqs, key=lambda x: x[1], reverse=True)

unpacked_data = []
for sentence in penntree_sentences:
    lowercase_sent = ['<s>'] + [token.split('|')[0].lower() for token in sentence] + ['</s>']
    unpacked_data.extend(lowercase_sent)

def train_bigram(data, k=1):
    bigram_model = defaultdict(Counter)
    V = len(word_pos_dict)

    for i, word in enumerate(data[1:], 2):
        history = ' '.join(data[i-2:i-1])
        bigram_model[history][word] += 1

    for history, wordcounts in bigram_model.items():
        history_count = sum(wordcounts.values())
        for word, word_count in wordcounts.items():
            bigram_model[history][word] = (word_count + k)/(history_count + k*V)

        bigram_model[history] = defaultdict(lambda: k/(history_count + k*V), bigram_model[history])

    return bigram_model

def predict_sent_prob(sentence, model):
    sentence = ['<s>'] + sentence
    prob = 1.0

    for i, word in enumerate(sentence[1:], 2):
        history = ' '.join(sentence[i-2:i-1])
        prob *= model[history][word]

    return prob

if __name__ == "__main__":
    bigram_model = train_bigram(unpacked_data, k=0.01)
    dill.dump(bigram_model, open( "bigram_model.pkl", "wb" ))

    print(predict_sent_prob(['a', 'man', 'goes'], bigram_model))
    print(predict_sent_prob(['a', 'men', 'go'], bigram_model))
    print(predict_sent_prob(['the', 'men', 'go'], bigram_model))
    singular_words = [word[0] for word in pos_word_dict["VBZ"]]
    plural_words = [word[0] for word in pos_word_dict["VB"]]

    # print(singular_words)
    # for key, value in pos_word_dict.items():
    #     if key == "VBZ":
    #         print(key, value)
