from helpers import singular_words, plural_words, evaluate2, evaluate3
import dill
import torch
import cProfile, pstats, io
import collections

sentences = dill.load(open('number_sentences.pkl', 'rb'))
lm = torch.load('model.pt', map_location=lambda storage, loc: storage)
dictionary = dill.load(open('dict', 'rb'))

counter = { 'singular' : { 'same_number' : { '0' : [0,0],
                                          '1' : [0,0],
                                          '2' : [0,0],
                                          '3' : [0,0],
                                          '4' : [0,0],
                                          '5' : [0,0]
                                        },
                        'other_number' : { '0' : [0,0],
                                           '1' : [0,0],
                                           '2' : [0,0],
                                           '3' : [0,0],
                                           '4' : [0,0],
                                           '5' : [0,0]
                                        },
                        'none' : { '0' : [0,0],
                                   '1' : [0,0],
                                   '2' : [0,0],
                                   '3' : [0,0],
                                   '4' : [0,0],
                                   '5' : [0,0],
                                   '6' : [0,0],
                                   '7' : [0,0],
                                   '8' : [0,0],
                                   '9' : [0,0],
                                   '10' : [0,0],
                                   '11' : [0,0],
                                   '12' : [0,0],
                                   '13' : [0,0],
                                   '14' : [0,0],
                                   '15' : [0,0]
                                 }
                    },
         'plural' : { 'same_number' : { '0' : [0,0],
                                        '1' : [0,0],
                                        '2' : [0,0],
                                        '3' : [0,0],
                                        '4' : [0,0],
                                        '5' : [0,0]
                                      },
                      'other_number' : { '0' : [0,0],
                                         '1' : [0,0],
                                         '2' : [0,0],
                                         '3' : [0,0],
                                         '4' : [0,0],
                                         '5' : [0,0]
                                       },
                      'none' : { '0' : [0,0],
                                 '1' : [0,0],
                                 '2' : [0,0],
                                 '3' : [0,0],
                                 '4' : [0,0],
                                 '5' : [0,0],
                                 '6' : [0,0],
                                 '7' : [0,0],
                                 '8' : [0,0],
                                 '9' : [0,0],
                                 '10' : [0,0],
                                 '11' : [0,0],
                                 '12' : [0,0],
                                 '13' : [0,0],
                                 '14' : [0,0],
                                 '15' : [0,0]
                               }
                   }

}

def standard_function():
    global counter
    for number in sentences:
        print("loop:", number)
        for sim in sentences[number]:
            print("attractor:", sim)
            for length in sentences[number][sim]:
                print("Length:", length)
                totala = len(sentences[number][sim][length])
                count = 0
                print("Number of sentences:", totala)
                for sentence in sentences[number][sim][length]:
                    count += 1
                    print("Sentence", count, "out of", totala)
                    counter[number][sim][length]\
                           [evaluate2(lm, dictionary, sentence)] +=1
        print("\n##############\n")

def shorter_function():
    global counter
    number = "singular"
    sim = "same_number"
    length = "3"
    count = 0
    totala = len(sentences[number][sim][length])
    for sentence in sentences[number][sim][length]:
        count += 1
        print("Sentence", count, "out of", totala)
        counter[number][sim][length]\
               [evaluate2(lm, dictionary, sentence)] +=1
        print("\n##############\n")

def count_to_n(n):
    global counter
    for number in sentences:
        print("loop:", number)
        for sim in sentences[number]:
            print("attractor:", sim)
            for length in sentences[number][sim]:
                print("Length:", length)
                totala = len(sentences[number][sim][length])
                count = 0
                print("Number of sentences:", totala)
                for sentence in sentences[number][sim][length]:
                    count += 1
                    print("Sentence", count, "out of", totala)
                    counter[number][sim][length]\
                           [evaluate2(lm, dictionary, sentence)] +=1
                    if count == n:
                        print("\n##############\n")
                        break

def short_evaluate_n(n):
    global counter
    for number in sentences:
        print("loop:", number)
        for sim in sentences[number]:
            print("attractor:", sim)
            for length in sentences[number][sim]:
                print("Length:", length)
                totala = len(sentences[number][sim][length])
                count = 0
                print("Number of sentences:", totala)
                for sentence in sentences[number][sim][length]:
                    count += 1
                    print("Sentence", count, "out of", totala)
                    counter[number][sim][length]\
                           [evaluate3(lm, dictionary, sentence)] +=1
                    if count == n:
                        print("\n##############\n")
                        break
        print("\n##############\n")
# evaluate2(lm, dictionary, sentence)

n = 1000
short_evaluate_n(n)
dill.dump(counter, open( "short_counter_{}.pkl".format(n), "wb" ))

# pr = cProfile.Profile()
# pr.enable()
# evaluate2(lm, dictionary, "the partner")
# pr.disable()
# s = io.StringIO()
# sortby = 'cumulative'
# ps = pstats.Stats(pr, stream=s).sort_stats(sortby)
# ps.print_stats()
# print(s.getvalue())
