from helpers import generate_sentence
import dill
# Create dataset with NN and NNS with in-between words for probability calculations or correct verb.

bigram_model = dill.load(open('bigram_model.pkl', 'rb'))
tags = { 'singular' : { 'same_number' : { '0' : ['DT', 'NN'],
                                          '1' : ['DT', 'NN', 'IN', 'DT', 'NN'],
                                          '2' : ['DT', 'NN', 'IN', 'DT', 'NN', 'IN', 'DT', 'NN'],
                                          '3' : ['DT', 'NN', 'IN', 'DT', 'NN', 'IN', 'DT', 'NN', 'IN', 'DT', 'NN'],
                                          '4' : ['DT', 'NN', 'IN', 'DT', 'NN', 'IN', 'DT', 'NN', 'IN', 'DT', 'NN', 'IN', 'DT', 'NN'],
                                          '5' : ['DT', 'NN', 'IN', 'DT', 'NN', 'IN', 'DT', 'NN', 'IN', 'DT', 'NN', 'IN', 'DT', 'NN', 'IN', 'DT', 'NN']
                                        },
                        'other_number' : { '0' : ['DT', 'NN'],
                                           '1' : ['DT', 'NN', 'IN', 'DT', 'NNS'],
                                           '2' : ['DT', 'NN', 'IN', 'DT', 'NNS', 'IN', 'DT', 'NNS'],
                                           '3' : ['DT', 'NN', 'IN', 'DT', 'NNS', 'IN', 'DT', 'NNS', 'IN', 'DT', 'NNS'],
                                           '4' : ['DT', 'NN', 'IN', 'DT', 'NNS', 'IN', 'DT', 'NNS', 'IN', 'DT', 'NNS', 'IN', 'DT', 'NNS'],
                                           '5' : ['DT', 'NN', 'IN', 'DT', 'NNS', 'IN', 'DT', 'NNS', 'IN', 'DT', 'NNS', 'IN', 'DT', 'NNS', 'IN', 'DT', 'NNS']
                                        },
                        'none' : { '0' : ['DT', 'NN'],
                                   '1' : ['DT', 'NN', 'RB'],
                                   '2' : ['DT', 'NN', 'RB', 'RB'],
                                   '3' : ['DT', 'NN', 'RB', 'CC', 'RB'],
                                   '4' : ['DT', 'NN', 'RB', 'RB', 'CC', 'RB'],
                                   '5' : ['DT', 'NN', 'RB', 'CC', 'RB', 'CC', 'RB'],
                                   '6' : ['DT', 'NN', 'RB', 'RB', 'CC', 'RB', 'CC', 'RB'],
                                   '7' : ['DT', 'NN', 'RB', 'CC', 'RB', 'CC', 'RB', 'CC', 'RB'],
                                   '8' : ['DT', 'NN', 'RB', 'RB', 'CC', 'RB', 'CC', 'RB', 'CC', 'RB'],
                                   '9' : ['DT', 'NN', 'RB', 'CC', 'RB', 'CC', 'RB', 'CC', 'RB', 'CC', 'RB'],
                                   '10' : ['DT', 'NN', 'RB', 'RB', 'CC', 'RB', 'CC', 'RB', 'CC', 'RB', 'CC', 'RB'],
                                   '11' : ['DT', 'NN', 'RB', 'CC', 'RB', 'CC', 'RB', 'CC', 'RB', 'CC', 'RB', 'CC', 'RB'],
                                   '12' : ['DT', 'NN', 'RB', 'RB', 'CC', 'RB', 'CC', 'RB', 'CC', 'RB', 'CC', 'RB', 'CC', 'RB'],
                                   '13' : ['DT', 'NN', 'RB', 'CC', 'RB', 'CC', 'RB', 'CC', 'RB', 'CC', 'RB', 'CC', 'RB', 'CC', 'RB'],
                                   '14' : ['DT', 'NN', 'RB', 'RB', 'CC', 'RB', 'CC', 'RB', 'CC', 'RB', 'CC', 'RB', 'CC', 'RB', 'CC', 'RB'],
                                   '15' : ['DT', 'NN', 'RB', 'CC', 'RB', 'CC', 'RB', 'CC', 'RB', 'CC', 'RB', 'CC', 'RB', 'CC', 'RB', 'CC', 'RB']
                                 }
                    },
         'plural' : { 'same_number' : { '0' : ['DT', 'NNS'],
                                        '1' : ['DT', 'NNS', 'IN', 'DT', 'NNS'],
                                        '2' : ['DT', 'NNS', 'IN', 'DT', 'NNS', 'IN', 'DT', 'NNS'],
                                        '3' : ['DT', 'NNS', 'IN', 'DT', 'NNS', 'IN', 'DT', 'NNS', 'IN', 'DT', 'NNS'],
                                        '4' : ['DT', 'NNS', 'IN', 'DT', 'NNS', 'IN', 'DT', 'NNS', 'IN', 'DT', 'NNS', 'IN', 'DT',  'NNS'],
                                        '5' : ['DT', 'NNS', 'IN', 'DT', 'NNS', 'IN', 'DT', 'NNS', 'IN', 'DT', 'NNS', 'IN', 'DT', 'NNS', 'IN', 'DT', 'NNS']
                                      },
                      'other_number' : { '0' : ['DT', 'NNS'],
                                         '1' : ['DT', 'NNS', 'IN', 'DT', 'NN'],
                                         '2' : ['DT', 'NNS', 'IN', 'DT', 'NN', 'IN', 'DT', 'NN'],
                                         '3' : ['DT', 'NNS', 'IN', 'DT', 'NN', 'IN', 'DT', 'NN', 'IN', 'DT', 'NN'],
                                         '4' : ['DT', 'NNS', 'IN', 'DT', 'NN', 'IN', 'DT', 'NN', 'IN', 'DT', 'NN', 'IN', 'DT', 'NN'],
                                         '5' : ['DT', 'NNS', 'IN', 'DT', 'NN', 'IN', 'DT', 'NN', 'IN', 'DT', 'NN', 'IN', 'DT', 'NN', 'IN', 'DT', 'NN']
                                       },
                      'none' : { '0' : ['DT', 'NNS'],
                                 '1' : ['DT', 'NNS', 'RB'],
                                 '2' : ['DT', 'NNS', 'RB', 'RB'],
                                 '3' : ['DT', 'NNS', 'RB', 'CC', 'RB'],
                                 '4' : ['DT', 'NNS', 'RB', 'RB', 'CC', 'RB'],
                                 '5' : ['DT', 'NNS', 'RB', 'CC', 'RB', 'CC', 'RB'],
                                 '6' : ['DT', 'NNS', 'RB', 'RB', 'CC', 'RB', 'CC', 'RB'],
                                 '7' : ['DT', 'NNS', 'RB', 'CC', 'RB', 'CC', 'RB', 'CC', 'RB'],
                                 '8' : ['DT', 'NNS', 'RB', 'RB', 'CC', 'RB', 'CC', 'RB', 'CC', 'RB'],
                                 '9' : ['DT', 'NNS', 'RB', 'CC', 'RB', 'CC', 'RB', 'CC', 'RB', 'CC', 'RB'],
                                 '10' : ['DT', 'NNS', 'RB', 'RB', 'CC', 'RB', 'CC', 'RB', 'CC', 'RB', 'CC', 'RB'],
                                 '11' : ['DT', 'NNS', 'RB', 'CC', 'RB', 'CC', 'RB', 'CC', 'RB', 'CC', 'RB', 'CC', 'RB'],
                                 '12' : ['DT', 'NNS', 'RB', 'RB', 'CC', 'RB', 'CC', 'RB', 'CC', 'RB', 'CC', 'RB', 'CC', 'RB'],
                                 '13' : ['DT', 'NNS', 'RB', 'CC', 'RB', 'CC', 'RB', 'CC', 'RB', 'CC', 'RB', 'CC', 'RB', 'CC', 'RB'],
                                 '14' : ['DT', 'NNS', 'RB', 'RB', 'CC', 'RB', 'CC', 'RB', 'CC', 'RB', 'CC', 'RB', 'CC', 'RB', 'CC', 'RB'],
                                 '15' : ['DT', 'NNS', 'RB', 'CC', 'RB', 'CC', 'RB', 'CC', 'RB', 'CC', 'RB', 'CC', 'RB', 'CC', 'RB', 'CC', 'RB']
                               }
                   }

}

sentences = {}
print_every = 10

def add_new_sentence(n, number, attractors):
    global sentences
    new_sentence = " ".join(generate_sentence(bigram_model, *tags[number][attractors][str(n)]))
    try:
        while sorted(sentences[number][attractors][str(n)] + [new_sentence]) != \
              sorted(list(set(sentences[number][attractors][str(n)] + [new_sentence]))):
            new_sentence = " ".join(generate_sentence(bigram_model, *tags[number][attractors][str(n)]))
        sentences[number][attractors][str(n)].append(new_sentence)
    except KeyError:
        try:
            sentences[number][attractors][str(n)] = [new_sentence]
        except KeyError:
            try:
                sentences[number][attractors] = {str(n): [new_sentence]}
            except KeyError:
                sentences[number] = {attractors: {str(n): [new_sentence]}}

for i in range(0, 1000):
    # singular subject with intervening nouns of the same number
    add_new_sentence(5, 'singular', 'same_number')
    add_new_sentence(4, 'singular', 'same_number')
    add_new_sentence(3, 'singular', 'same_number')
    add_new_sentence(2, 'singular', 'same_number')
    add_new_sentence(1, 'singular', 'same_number')
    add_new_sentence(0, 'singular', 'same_number')

    # singular subject with intervening nouns of the other number
    add_new_sentence(5, 'singular', 'other_number')
    add_new_sentence(4, 'singular', 'other_number')
    add_new_sentence(3, 'singular', 'other_number')
    add_new_sentence(2, 'singular', 'other_number')
    add_new_sentence(1, 'singular', 'other_number')
    add_new_sentence(0, 'singular', 'other_number')

    # singular subject with no intervening nouns but increasing distance
    add_new_sentence(15, 'singular', 'none')
    add_new_sentence(14, 'singular', 'none')
    add_new_sentence(13, 'singular', 'none')
    add_new_sentence(12, 'singular', 'none')
    add_new_sentence(11, 'singular', 'none')
    add_new_sentence(10, 'singular', 'none')
    add_new_sentence(9, 'singular', 'none')
    add_new_sentence(8, 'singular', 'none')
    add_new_sentence(7, 'singular', 'none')
    add_new_sentence(6, 'singular', 'none')
    add_new_sentence(5, 'singular', 'none')
    add_new_sentence(4, 'singular', 'none')
    add_new_sentence(3, 'singular', 'none')
    add_new_sentence(2, 'singular', 'none')
    add_new_sentence(1, 'singular', 'none')
    add_new_sentence(0, 'singular', 'none')

    # plural subject with intervening nouns of the same number
    add_new_sentence(5, 'plural', 'same_number')
    add_new_sentence(4, 'plural', 'same_number')
    add_new_sentence(3, 'plural', 'same_number')
    add_new_sentence(2, 'plural', 'same_number')
    add_new_sentence(1, 'plural', 'same_number')
    add_new_sentence(0, 'plural', 'same_number')

    # plural subject with intervening nouns of the other number
    add_new_sentence(5, 'plural', 'other_number')
    add_new_sentence(4, 'plural', 'other_number')
    add_new_sentence(3, 'plural', 'other_number')
    add_new_sentence(2, 'plural', 'other_number')
    add_new_sentence(1, 'plural', 'other_number')
    add_new_sentence(0, 'plural', 'other_number')

    # plural subject with no intervening nouns but increasing distance
    add_new_sentence(15, 'plural', 'none')
    add_new_sentence(14, 'plural', 'none')
    add_new_sentence(13, 'plural', 'none')
    add_new_sentence(12, 'plural', 'none')
    add_new_sentence(11, 'plural', 'none')
    add_new_sentence(10, 'plural', 'none')
    add_new_sentence(9, 'plural', 'none')
    add_new_sentence(8, 'plural', 'none')
    add_new_sentence(7, 'plural', 'none')
    add_new_sentence(6, 'plural', 'none')
    add_new_sentence(5, 'plural', 'none')
    add_new_sentence(4, 'plural', 'none')
    add_new_sentence(3, 'plural', 'none')
    add_new_sentence(2, 'plural', 'none')
    add_new_sentence(1, 'plural', 'none')
    add_new_sentence(0, 'plural', 'none')

    if i % print_every == 0:
        print(i)

dill.dump(sentences, open( "number_sentences.pkl", "wb" ))
print('done!')

# # Function to evaluate if verb prediction is in accordance to noun.
# import random
# check_words = ['is', 'are']
# count_results=[]
#
# def ev(sentence, pos_word_dict, check_words,lm,dictionary,count_results):
#     test = sentence.split()
#     word = test[1]
#
#     if word in [item[0] for item in pos_word_dict['NN']]:
#         tag='NN'
#     if word in [item[0] for item in pos_word_dict['NNS']]:
#         tag='NNS'
#     if word in [item[0] for item in pos_word_dict['NN']] and word in [item[0] for item in pos_word_dict['NNS']]:
#         p=0.5
#         num=random.random()
#         if num>=p:
#             tag='NN'
#         if num<p:
#             tag='NNS'
#
#     results = evaluate(lm, dictionary, sentence, check_words)
#
#     sentence=test
#
#     if tag=='NN':
#         if results[0] > results[1]:
#             count_results.append((1,len(sentence)-2))
#         else:
#             count_results.append((0,len(sentence)-2))
#     else:
#         if results[1] > results[0]:
#             count_results.append((1,len(sentence)-2))
#         else:
#             count_results.append((0,len(sentence)-2))
#
#     return count_results
#
# # Call function for all generated sentences and store success or fail alongside sentence length
#
# for sentence in db_missing:
#     res = ev(str(sentence), pos_word_dict, check_words,lm,dictionary,count_results)
#
#
#
# acc_0,acc_1,acc_2,acc_3,acc_4,acc_5,acc_6=[],[],[],[],[],[],[]
#
# for x,y in res:
#     if y==0:
#         acc_0.append(x)
#     if y==1:
#         acc_1.append(x)
#     if y==2:
#         acc_2.append(x)
#     if y==3:
#         acc_3.append(x)
#     if y==4:
#         acc_4.append(x)
#     if y==5:
#         acc_5.append(x)
#     else:
#         acc_6.append(x)
#
#
# import matplotlib.pyplot as plt
# import plotly.graph_objs as go
#
#
# f, axarr = plt.subplots(2, 4)
# axarr[0, 0].hist(acc_0)
# axarr[0, 0].set_title('No attractor')
# axarr[0, 1].hist(acc_1)
# axarr[0, 1].set_title('1 attractor')
# axarr[0, 2].hist(acc_2)
# axarr[0, 2].set_title('2 attractors')
# axarr[0, 3].hist(acc_3)
# axarr[0, 3].set_title('3 attractors')
# axarr[1, 0].hist(acc_4)
# axarr[1, 0].set_title('4 attractors')
# axarr[1, 1].hist(acc_5)
# axarr[1, 1].set_title('5 attractors')
# axarr[1, 2].hist(acc_6)
# axarr[1, 2].set_title('6 attractors')
# f.tight_layout()
#
# plt.show()
