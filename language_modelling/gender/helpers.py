from load import *

# load pytorch softmax function
softmax = nn.Softmax()

def evaluate(model, dictionary, sentence, check_words):
    # Turn on evaluation mode which disables dropout.
    model.eval()

    # number of tokens (= output size)
    ntokens = len(dictionary)
    hidden = model.init_hidden(1)

    # tokenise the sentence, put in torch Variable
    test_data = tokenise(sentence, dictionary)
    input_data = Variable(test_data, volatile=True)

    # run the model, compute probabilities by applying softmax
    output, hidden = model(input_data, hidden)
    output_flat = output.view(-1, ntokens)
    logits = output[-1, :]
    sm = softmax(logits).view(ntokens)

    # get probabilities of certain words by looking up their
    # indices and print them
    def get_prob(word):
        return sm[dictionary.word2idx[word]].data[0]

    print(sentence, '\n')
    print('\n'.join(
            ['%s: %f' % (word, get_prob(word)) for word in check_words]
            ))
    return

def evaluate_gender_reflexive(model, dictionary, sentence):
    """Example function with types documented in the docstring.

    `PEP 484`_ type annotations are supported. If attribute, parameter, and
    return types are annotated according to `PEP 484`_, they do not need to be
    included in the docstring:

    Args:
        model (Pytorch model): A language model in pytorch, in our case a RNN.
        dictionary (???): ???
        sentence (???): ???

    Returns:
        int: 1 for plural, 0 for singular.

    """

    # Turn on evaluation mode which disables dropout.
    model.eval()

    # number of tokens (= output size)
    ntokens = len(dictionary)
    hidden = model.init_hidden(1)

    # tokenise the sentence, put in torch Variable
    test_data = tokenise(sentence, dictionary)
    input_data = Variable(test_data, volatile=True)

    # run the model, compute probabilities by applying softmax
    output, hidden = model(input_data, hidden)
    output_flat = output.view(-1, ntokens)
    logits = output[-1, :]
    sm = softmax(logits).view(ntokens)

    # get probabilities of certain words by looking up their
    # indices and print them
    def get_prob(word):
        return sm[dictionary.word2idx[word]].data[0]

    # print(len(plural_words), len(singular_words))
    if (get_prob("herself")/ (1 - 0.01*(len(sentence)/11))) > (get_prob("himself") / 8*(1 - 0.01*(len(sentence)/11))):
        return 1
    else:
        return 0

def evaluate_gender_possessive(model, dictionary, sentence):
    """Example function with types documented in the docstring.

    `PEP 484`_ type annotations are supported. If attribute, parameter, and
    return types are annotated according to `PEP 484`_, they do not need to be
    included in the docstring:

    Args:
        model (Pytorch model): A language model in pytorch, in our case a RNN.
        dictionary (???): ???
        sentence (???): ???

    Returns:
        int: 1 for plural, 0 for singular.

    """

    # Turn on evaluation mode which disables dropout.
    model.eval()

    # number of tokens (= output size)
    ntokens = len(dictionary)
    hidden = model.init_hidden(1)

    # tokenise the sentence, put in torch Variable
    test_data = tokenise(sentence, dictionary)
    input_data = Variable(test_data, volatile=True)

    # run the model, compute probabilities by applying softmax
    output, hidden = model(input_data, hidden)
    output_flat = output.view(-1, ntokens)
    logits = output[-1, :]
    sm = softmax(logits).view(ntokens)

    # get probabilities of certain words by looking up their
    # indices and print them
    def get_prob(word):
        return sm[dictionary.word2idx[word]].data[0]

    if (get_prob("her") / (1 - 0.01*(len(sentence)/11))) > ((get_prob("his") + get_prob("him")) / 5.2*(1 - 0.01*(len(sentence)/11))):
        return 1
    else:
        return 0
