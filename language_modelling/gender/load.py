import torch
from torch.autograd import Variable
import torch.nn as nn
import pickle
import dill
import numpy as np
from itertools import product
from collections import defaultdict, Counter

gender_sentences = {}
dictionary = pickle.load(open('dict', 'rb'))
max_seq_len = 50

# function to transform sentence into word id's and put them in a pytorch Variable
# NB Assumes the sentence is already tokenised!
def tokenise(sentence, dictionary):
    words = sentence.split(' ')
    l = len(words)
    assert l <= max_seq_len, "sentence too long"
    token = 0
    ids = torch.LongTensor(l)

    for word in words:
        try:
            ids[token] = dictionary.word2idx[word]
        except KeyError:
            print( word)
            raw_input()
            ids[token] = dictionary.word2idx['<unk>']
        token += 1
    return ids

sg_DTs = ['that', 'this', 'the', 'a', 'an', 'any', 'every', 'some']

pronouns = ['he', 'she', 'his', 'her', 'himself', 'herself']

male_nouns = ['brother', 'husband', 'father', 'boy', 'man', \
'king', 'lord', 'actor', 'son', ]

female_nouns = ['sister', 'wife', 'mother', 'girl', 'woman', \
'queen', 'lady', 'actress', 'daughter']

reflexive_verbs = ['enjoyed', 'enjoys', 'prepares', 'prepared', \
'killed', 'cuts', 'cut', 'hurt', 'introduced', 'teaches', 'taught', \
'applies', 'applied', 'behaved', 'blames', 'blamed', 'expressed', 'finds', \
'found', 'helps', 'helped', 'sees', 'saw', 'invites', 'invited']

penntree_sentences = [sentence.split() for sentence in open('../sec02-21.gold.tagged.txt', 'r').readlines()]

word_pos_dict_gender = {}
for sentence in penntree_sentences:
    for token in sentence:
        word = token.split('|')[0].lower()
        pos = token.split('|')[1]
        if word in pronouns:
            pos = word.upper()
        elif word in male_nouns:
            pos = 'MNN'
        elif word in female_nouns:
            pos = 'FNN'
        elif word in reflexive_verbs:
            pos = 'VBR'
        elif word == 'of':
            pos = 'OF'
        if word not in dictionary.word2idx:
            continue
        else:
            if word in word_pos_dict_gender:
                if pos in word_pos_dict_gender[word]:
                    word_pos_dict_gender[word][pos] += 1
                else:
                    word_pos_dict_gender[word][pos] = 1
            else:
                word_pos_dict_gender[word] = {pos: 1}

pos_word_dict_gender = {}
for word in word_pos_dict_gender:
    # skip the words that are not in the model dictionary
    if word not in dictionary.word2idx:
        continue
    else:
        for pos, count in word_pos_dict_gender[word].items():
            if pos in pos_word_dict_gender:
                if word in pos_word_dict_gender[pos]:
                    pos_word_dict_gender[pos][word] += count
                else:
                    pos_word_dict_gender[pos][word] = count
            else:
                pos_word_dict_gender[pos] = {word: count}

for pos in pos_word_dict_gender:
    total = sum([count for word, count in pos_word_dict_gender[pos].items()])
    word_freqs = []
    for word, count in pos_word_dict_gender[pos].items():
        word_freq = (word, (count/total))
        word_freqs.append(word_freq)
    pos_word_dict_gender[pos] = sorted(word_freqs, key=lambda x: x[1], reverse=True)

def w(tag):
    """
    Gets a word from the pos_word_dict_gender with the specified tag.
    """
    if tag == 'DT':
        word = np.random.choice(sg_DTs)
    else:
        word = np.random.choice([word for word, prob in pos_word_dict_gender[tag]], 1,
                                 p=[prob for word, prob in pos_word_dict_gender[tag]])[0]
    return word

def generate_gender_sentences(n):
    global gender_sentences

    ################
    ## REFLEXIVES ##
    ################

    gender_sentences['reflexive'] = {'female': {'same': {}, 'other': {}, 'distance': {}},
                                     'male': {'same': {}, 'other': {}, 'distance': {}}}

    ### FEMALE ###
    # female subject with directly following reflexive:
    # 'she kills [herself]'
    gender_sentences['reflexive']['female']['0'] = [' '.join([dt, noun, verb]) \
        for dt, noun, verb in product(sg_DTs, female_nouns, reflexive_verbs)] + \
        ['she ' + verb for verb in reflexive_verbs]
    np.random.shuffle(gender_sentences['reflexive']['female']['0'])
    gender_sentences['reflexive']['female']['0'] = gender_sentences['reflexive']['female']['0'][:n]

    # female subject sentences with female attractors
    # 2: 'she kills her mother [herself]'
    # 4: 'she kills the sister and the daughter of her aunt [herself]'
    gender_sentences['reflexive']['female']['same']['2'] = [' '.join([w('DT'), w('FNN'), w('VBR'), w('HER'), w('FNN')]) \
        for i in range(n//2)] + [' '.join(['she', w('VBR'), w('HER'), w('FNN')]) for i in range(n//2)]
    gender_sentences['reflexive']['female']['same']['4'] = [' '.join([w('DT'), w('FNN'), w('VBR'), w('DT'), w('FNN'), \
        w('CC'), w('DT'), w('FNN'), w('OF'), w('HER'), w('FNN')]) for i in range(n//2)] + [' '.join(['she', w('VBR'), w('DT'), \
        w('FNN'), w('CC'), w('DT'), w('FNN'), w('OF'), w('HER'), w('FNN')]) for i in range(n//2)]
    np.random.shuffle(gender_sentences['reflexive']['female']['same']['2'])
    np.random.shuffle(gender_sentences['reflexive']['female']['same']['4'])

    # female subject sentences with male attractors
    # 2: 'she kills her father [herself]'
    # 4: 'she kills the brother and the son of his uncle [herself]'
    gender_sentences['reflexive']['female']['other']['2'] = [' '.join([w('DT'), w('FNN'), w('VBR'), w('HIS'), w('MNN')]) \
        for i in range(n//2)] + [' '.join(['she', w('VBR'), w('HIS'), w('MNN')]) for i in range(n//2)]
    gender_sentences['reflexive']['female']['other']['4'] = [' '.join([w('DT'), w('FNN'), w('VBR'), w('DT'), w('MNN'), \
        w('CC'), w('DT'), w('MNN'), w('OF'), w('HIS'), w('MNN')]) for i in range(n//2)] + [' '.join(['she', w('VBR'), w('DT'), \
        w('MNN'), w('CC'), w('DT'), w('MNN'), w('OF'), w('HIS'), w('MNN')]) for i in range(n//2)]
    np.random.shuffle(gender_sentences['reflexive']['female']['other']['2'])
    np.random.shuffle(gender_sentences['reflexive']['female']['other']['4'])

    # female subject sentences with no attractors but larger distance
    # 3: 'she helps or enjoys [herself]'
    # 9: 'she helps and enjoys or cuts and kills and blames [herself]'
    gender_sentences['reflexive']['female']['distance']['3'] = [' '.join([w('DT'), w('FNN'), w('VBR'), w('CC'), w('VBR')]) \
        for i in range(n//2)] + [' '.join(['she', w('VBR'), w('CC'), w('VBR')]) for i in range(n//2)]
    gender_sentences['reflexive']['female']['distance']['9'] = [' '.join([w('DT'), w('FNN'), w('VBR'), w('CC'), w('VBR'), \
        w('CC'), w('VBR'), w('CC'), w('VBR'), w('CC'), w('VBR')]) for i in range(n//2)] + [' '.join(['she', w('VBR'), w('CC'), \
        w('VBR'), w('CC'), w('VBR'), w('CC'), w('VBR'), w('CC'), w('VBR')]) for i in range(n//2)]
    np.random.shuffle(gender_sentences['reflexive']['female']['distance']['3'])
    np.random.shuffle(gender_sentences['reflexive']['female']['distance']['9'])

    ### MALE ###
    # male subject with directly folowing reflexive:
    # 'he kills [himself]'
    gender_sentences['reflexive']['male']['0'] = [' '.join([dt, noun, verb]) \
    for dt, noun, verb in product(sg_DTs, male_nouns, reflexive_verbs)] + \
    ['he ' + verb for verb in reflexive_verbs]
    np.random.shuffle(gender_sentences['reflexive']['male']['0'])
    gender_sentences['reflexive']['male']['0'] = gender_sentences['reflexive']['male']['0'][:n]

    # male subject sentences with male attractors
    # 2: 'he kills his father [himself]'
    # 4: 'he kills the brother and the son of his uncle [himself]'
    gender_sentences['reflexive']['male']['same']['2'] = [' '.join([w('DT'), w('MNN'), w('VBR'), w('HIS'), w('MNN')]) \
        for i in range(n//2)] + [' '.join(['he', w('VBR'), w('HIS'), w('MNN')]) for i in range(n//2)]
    gender_sentences['reflexive']['male']['same']['4'] = [' '.join([w('DT'), w('MNN'), w('VBR'), w('DT'), w('MNN'), \
        w('CC'), w('DT'), w('MNN'), w('OF'), w('HIS'), w('MNN')]) for i in range(n//2)] + [' '.join(['he', w('VBR'), w('DT'), \
        w('MNN'), w('CC'), w('DT'), w('MNN'), w('OF'), w('HIS'), w('MNN')]) for i in range(n//2)]
    np.random.shuffle(gender_sentences['reflexive']['male']['same']['2'])
    np.random.shuffle(gender_sentences['reflexive']['male']['same']['4'])

    # male subject sentences with female attractors
    # 2: 'he kills her mother [himself]'
    # 4: 'he kills the sister and the daughter of her aunt [himself]'
    gender_sentences['reflexive']['male']['other']['2'] = [' '.join([w('DT'), w('MNN'), w('VBR'), w('HER'), w('FNN')]) \
        for i in range(n//2)] + [' '.join(['he', w('VBR'), w('HER'), w('FNN')]) for i in range(n//2)]
    gender_sentences['reflexive']['male']['other']['4'] = [' '.join([w('DT'), w('MNN'), w('VBR'), w('DT'), w('FNN'), \
        w('CC'), w('DT'), w('FNN'), w('OF'), w('HER'), w('FNN')]) for i in range(n//2)] + [' '.join(['he', w('VBR'), w('DT'), \
        w('FNN'), w('CC'), w('DT'), w('FNN'), w('OF'), w('HER'), w('FNN')]) for i in range(n//2)]
    np.random.shuffle(gender_sentences['reflexive']['male']['other']['2'])
    np.random.shuffle(gender_sentences['reflexive']['male']['other']['4'])

    # male subject sentences with no attractors but larger distance
    # 3: 'he helps or enjoys [himself]'
    # 9: 'he helps and enjoys or cuts and kills and blames [himself]'
    gender_sentences['reflexive']['male']['distance']['3'] = [' '.join([w('DT'), w('MNN'), w('VBR'), w('CC'), w('VBR')]) \
        for i in range(n//2)] + [' '.join(['he', w('VBR'), w('CC'), w('VBR')]) for i in range(n//2)]
    gender_sentences['reflexive']['male']['distance']['9']= [' '.join([w('DT'), w('MNN'), w('VBR'), w('CC'), w('VBR'), \
        w('CC'), w('VBR'), w('CC'), w('VBR'), w('CC'), w('VBR')]) for i in range(n//2)] + [' '.join(['he', w('VBR'), w('CC'), \
        w('VBR'), w('CC'), w('VBR'), w('CC'), w('VBR'), w('CC'), w('VBR')]) for i in range(n//2)]
    np.random.shuffle(gender_sentences['reflexive']['male']['distance']['3'])
    np.random.shuffle(gender_sentences['reflexive']['male']['distance']['9'])

    #################
    ## POSSESSIVES ##
    #################

    gender_sentences['possessive'] = {'female': {'same': {}, 'other': {}, 'distance': {}},
                                     'male': {'same': {}, 'other': {}, 'distance': {}}}

    # sentences for the directly following condition are the same as for the reflexives
    # e.g. '(s)he kills [him/her]'
    gender_sentences['possessive']['female']['0'] = gender_sentences['reflexive']['female']['0']
    gender_sentences['possessive']['male']['0'] = gender_sentences['reflexive']['male']['0']

    # sentences for the distance conditions are also the same
    # e.g. 9: '(s)he finds and invites and cuts and kills and enjoys [him/her]' (lol)
    gender_sentences['possessive']['female']['distance']['3'] = gender_sentences['reflexive']['female']['distance']['3']
    gender_sentences['possessive']['female']['distance']['9'] = gender_sentences['reflexive']['female']['distance']['9']
    gender_sentences['possessive']['male']['distance']['3'] = gender_sentences['reflexive']['male']['distance']['3']
    gender_sentences['possessive']['male']['distance']['9'] = gender_sentences['reflexive']['male']['distance']['9']

    ### FEMALE ###

    # female subject sentences with female attractors
    # 2: 'she invites the sister and mother of [her/his]'
    # 4: 'she invites the sister and mother or daughter and aunt of [her/his]'
    gender_sentences['possessive']['female']['same']['2'] = [' '.join([w('DT'), w('FNN'), w('VBR'), w('DT'), w('FNN'), w('CC'), \
    w('FNN'), w('OF')]) for i in range(n//2)] + [' '.join(['she', w('VBR'), w('DT'), w('FNN'), w('CC'), w('FNN'), w('OF')]) \
    for i in range(n//2)]
    gender_sentences['possessive']['female']['same']['4'] = [' '.join([w('DT'), w('FNN'), w('VBR'), w('DT'), w('FNN'), w('CC'), \
    w('FNN'), w('CC'), w('FNN'), w('CC'), w('FNN'), w('OF')]) for i in range(n//2)] + [' '.join(['she', w('VBR'), w('DT'), \
    w('FNN'), w('CC'), w('FNN'), w('CC'), w('FNN'), w('CC'), w('FNN'), w('OF')]) for i in range(n//2)]
    np.random.shuffle(gender_sentences['possessive']['female']['same']['2'])
    np.random.shuffle(gender_sentences['possessive']['female']['same']['4'])

    # female subject sentences with male attractors
    # 2: 'she invites the brother and father of [her/his]'
    # 4: 'she invites the brother and father or son and uncle of [her/his]'
    gender_sentences['possessive']['female']['other']['2'] = [' '.join([w('DT'), w('FNN'), w('VBR'), w('DT'), w('MNN'), w('CC'), \
    w('MNN'), w('OF')]) for i in range(n//2)] + [' '.join(['she', w('VBR'), w('DT'), w('MNN'), w('CC'), w('MNN'), w('OF')]) \
    for i in range(n//2)]
    gender_sentences['possessive']['female']['other']['4'] = [' '.join([w('DT'), w('FNN'), w('VBR'), w('DT'), w('MNN'), w('CC'), \
    w('MNN'), w('CC'), w('MNN'), w('CC'), w('MNN'), w('OF')]) for i in range(n//2)] + [' '.join(['she', w('VBR'), w('DT'), \
    w('MNN'), w('CC'), w('MNN'), w('CC'), w('MNN'), w('CC'), w('MNN'), w('OF')]) for i in range(n//2)]
    np.random.shuffle(gender_sentences['possessive']['female']['other']['2'])
    np.random.shuffle(gender_sentences['possessive']['female']['other']['4'])

    ### MALE ###

    # male subject sentences with male attractors
    # 2: 'he invites the brother and father of [her/his]'
    # 4: 'he invites the brother and father or son and uncle of [her/his]'
    gender_sentences['possessive']['male']['same']['2'] = [' '.join([w('DT'), w('MNN'), w('VBR'), w('DT'), w('MNN'), w('CC'), \
    w('MNN'), w('OF')]) for i in range(n//2)] + [' '.join(['he', w('VBR'), w('DT'), w('MNN'), w('CC'), w('MNN'), w('OF')]) \
    for i in range(n//2)]
    gender_sentences['possessive']['male']['same']['4'] = [' '.join([w('DT'), w('MNN'), w('VBR'), w('DT'), w('MNN'), w('CC'), \
    w('MNN'), w('CC'), w('MNN'), w('CC'), w('MNN'), w('OF')]) for i in range(n//2)] + [' '.join(['he', w('VBR'), w('DT'), \
    w('MNN'), w('CC'), w('MNN'), w('CC'), w('MNN'), w('CC'), w('MNN'), w('OF')]) for i in range(n//2)]
    np.random.shuffle(gender_sentences['possessive']['male']['same']['2'])
    np.random.shuffle(gender_sentences['possessive']['male']['same']['4'])

    # male subject sentences with female attractors
    # 2: 'he invites the sister and mother of [her/his]'
    # 4: 'he invites the sister and mother or daughter and aunt of [her/his]'
    gender_sentences['possessive']['male']['other']['2'] = [' '.join([w('DT'), w('MNN'), w('VBR'), w('DT'), w('FNN'), w('CC'), \
    w('FNN'), w('OF')]) for i in range(n//2)] + [' '.join(['he', w('VBR'), w('DT'), w('FNN'), w('CC'), w('FNN'), w('OF')]) \
    for i in range(n//2)]
    gender_sentences['possessive']['male']['other']['4'] = [' '.join([w('DT'), w('MNN'), w('VBR'), w('DT'), w('FNN'), w('CC'), \
    w('FNN'), w('CC'), w('FNN'), w('CC'), w('FNN'), w('OF')]) for i in range(n//2)] + [' '.join(['he', w('VBR'), w('DT'), \
    w('FNN'), w('CC'), w('FNN'), w('CC'), w('FNN'), w('CC'), w('FNN'), w('OF')]) for i in range(n//2)]
    np.random.shuffle(gender_sentences['possessive']['male']['other']['2'])
    np.random.shuffle(gender_sentences['possessive']['male']['other']['4'])

if __name__ == "__main__":
    generate_gender_sentences(1000)
    print(len(gender_sentences['possessive']['male']['other']['2']), len(gender_sentences['possessive']['male']['0']))
    dill.dump(gender_sentences, open('gender_sentences.pkl', 'wb'))
    print('done!')
