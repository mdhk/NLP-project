from helpers import evaluate_gender_possessive, evaluate_gender_reflexive
import dill
import torch
import collections
import copy

gender_sentences = dill.load(open('gender_sentences.pkl', 'rb'))
lm = torch.load('../model.pt', map_location=lambda storage, loc: storage)
dictionary = dill.load(open('dict', 'rb'))

gender_counter = { 'reflexive' : { 'female' : { '0' : [0, 0],
                                                'same' : {'2' : [0, 0], '4': [0, 0]},
                                                'other' : {'2' : [0, 0], '4': [0, 0]},
                                                'distance' : {'3': [0, 0], '9': [0, 0]}
                                              },
                                    'male' : { '0' : [0, 0],
                                               'same' : {'2' : [0, 0], '4': [0, 0]},
                                               'other' : {'2' : [0, 0], '4': [0, 0]},
                                               'distance' : {'3': [0, 0], '9': [0, 0]}
                                             }
                                 },
                   'possessive' : { 'female' : { '0' : [0, 0],
                                                 'same' : {'2' : [0, 0], '4': [0, 0]},                                  'other' : {'2' : [0, 0], '4': [0, 0]},
                                                 'distance' : {'3': [0, 0], '9': [0, 0]}
                                               },
                                     'male' : { '0' : [0, 0],
                                                'same' : {'2' : [0, 0], '4': [0, 0]},
                                                'other' : {'2' : [0, 0], '4': [0, 0]},
                                                'distance' : {'3': [0, 0], '9': [0, 0]}
                                              }
                                   }
}

def short_evaluate_n(n):
    global gender_counter
    for pronoun_type in gender_sentences:
        # choose the right evaluate function
        if pronoun_type == 'reflexive':
            evaluate = evaluate_gender_reflexive
        else:
            evaluate = evaluate_gender_possessive

        for gender in gender_sentences[pronoun_type]:

            for attractor in gender_sentences[pronoun_type][gender]:
                if attractor == '0':
                    count = 0
                    print(pronoun_type, gender, attractor)

                    for sentence in gender_sentences[pronoun_type][gender][attractor]:
                        count += 1
                        if count % print_every == 0:
                            print(count)
                        gender_counter[pronoun_type][gender][attractor]\
                        [evaluate(lm, dictionary, sentence)] += 1
                        if count == n:
                            print("\n##############\n")
                            break
                else:
                    for length in gender_sentences[pronoun_type][gender][attractor]:
                        count = 0
                        print(pronoun_type, gender, attractor, length)

                        for sentence in gender_sentences[pronoun_type][gender][attractor][length]:
                            count += 1
                            if count % print_every == 0:
                                print(count)
                            gender_counter[pronoun_type][gender][attractor][length]\
                            [evaluate(lm, dictionary, sentence)] += 1
                            if count == n:
                                print("\n##############\n")
                                break

print_every = 100
n = 1000
short_evaluate_n(n)
dill.dump(gender_counter, open("gender_counter_{}_smoothed_true.pkl".format(n), "wb"))
