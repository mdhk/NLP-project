from load import *

# load pytorch softmax function
softmax = nn.Softmax()
singular_words = [word[0] for word in pos_word_dict["VBZ"]]
plural_words = [word[0] for word in pos_word_dict["VB"]]

def evaluate(model, dictionary, sentence, check_words):
    # Turn on evaluation mode which disables dropout.
    model.eval()

    # number of tokens (= output size)
    ntokens = len(dictionary)
    hidden = model.init_hidden(1)

    # tokenise the sentence, put in torch Variable
    test_data = tokenise(sentence, dictionary)
    input_data = Variable(test_data, volatile=True)

    # run the model, compute probabilities by applying softmax
    output, hidden = model(input_data, hidden)
    output_flat = output.view(-1, ntokens)
    logits = output[-1, :]
    sm = softmax(logits).view(ntokens)

    # get probabilities of certain words by looking up their
    # indices and print them
    def get_prob(word):
        return sm[dictionary.word2idx[word]].data[0]

    print(sentence, '\n')
    print('\n'.join(
            ['%s: %f' % (word, get_prob(word)) for word in check_words]
            ))
    return

def evaluate2(model, dictionary, sentence):
    """Example function with types documented in the docstring.

    `PEP 484`_ type annotations are supported. If attribute, parameter, and
    return types are annotated according to `PEP 484`_, they do not need to be
    included in the docstring:

    Args:
        model (Pytorch model): A language model in pytorch, in our case a RNN.
        dictionary (???): ???
        sentence (???): ???

    Returns:
        int: 1 for plural, 0 for singular.

    """

    # Turn on evaluation mode which disables dropout.
    model.eval()

    # number of tokens (= output size)
    ntokens = len(dictionary)
    hidden = model.init_hidden(1)

    # tokenise the sentence, put in torch Variable
    test_data = tokenise(sentence, dictionary)
    input_data = Variable(test_data, volatile=True)

    # run the model, compute probabilities by applying softmax
    output, hidden = model(input_data, hidden)
    output_flat = output.view(-1, ntokens)
    logits = output[-1, :]
    sm = softmax(logits).view(ntokens)

    # get probabilities of certain words by looking up their
    # indices and print them
    def get_prob(word):
        return sm[dictionary.word2idx[word]].data[0]

    singular_prob = 0
    for word in singular_words:
        singular_prob += get_prob(word)

    plural_prob = 0
    for word in plural_words:
        plural_prob += get_prob(word)

    # print(len(plural_words), len(singular_words))
    if plural_prob > singular_prob:
        return 1
    else:
        return 0

def evaluate3(model, dictionary, sentence):
    """Example function with types documented in the docstring.

    `PEP 484`_ type annotations are supported. If attribute, parameter, and
    return types are annotated according to `PEP 484`_, they do not need to be
    included in the docstring:

    Args:
        model (Pytorch model): A language model in pytorch, in our case a RNN.
        dictionary (???): ???
        sentence (???): ???

    Returns:
        int: 1 for plural, 0 for singular.

    """

    # Turn on evaluation mode which disables dropout.
    model.eval()

    # number of tokens (= output size)
    ntokens = len(dictionary)
    hidden = model.init_hidden(1)

    # tokenise the sentence, put in torch Variable
    test_data = tokenise(sentence, dictionary)
    input_data = Variable(test_data, volatile=True)

    # run the model, compute probabilities by applying softmax
    output, hidden = model(input_data, hidden)
    output_flat = output.view(-1, ntokens)
    logits = output[-1, :]
    sm = softmax(logits).view(ntokens)

    # get probabilities of certain words by looking up their
    # indices and print them
    def get_prob(word):
        return sm[dictionary.word2idx[word]].data[0]
        
    # print(len(plural_words), len(singular_words))
    if get_prob("are") > get_prob("is"):
        return 1
    else:
        return 0

def generate_sentence(bigram_model, *tags):
    """
    Returns a sentence (as a list of words)
    """
    sentence = []
    for tag in tags:
        # choose a word according to the probability distribution
        word = np.random.choice([word for word, prob in pos_word_dict[tag]], 1,
                                p=[prob for word, prob in pos_word_dict[tag]])[0]

        while (len(sentence) > 0) and (not tag in ['VB', 'VBZ']) and (bigram_model[sentence[-1]][word] < 1e-05):
            word = np.random.choice([word for word, prob in pos_word_dict[tag]], 1,
                                    p=[prob for word, prob in pos_word_dict[tag]])[0]
        # append this word to the sentence
        sentence.append(word)
    return sentence
